#include <stdio.h>
#include <sys/time.h>
#include <string.h>

#include "Drivers/BMA150Accelerometer.h"
#include "Drivers/Raw3DSensorData.h"
#include "RaspberryPiHardware/RaspberryPiI2C.h"

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

int I2CPort(int argc, char* argv[])
{
	for(int i = 1; i < argc; i++)
	{
		if(strcmp(argv[i], "-0") == 0)
			return 0;
	}
	return 1;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

void WriteCSV_UntilControlC(int port)
{
	RaspberryPiI2C_Initialise(port);

	printf("Time,Acceleration.X,Acceleration.Y,Acceleration.Z\n");

	struct timeval start;
	gettimeofday(&start, NULL);

	while(true)
	{
		struct Raw3DSensorData rawAcceleration = BMA150Accelerometer_ReadAcceleration();

		struct timeval current;
		gettimeofday(&current, NULL);

		const long seconds = current.tv_sec - start.tv_sec;
		const long useconds = current.tv_usec - start.tv_usec;
		const long elapsed = ((seconds) * 1000 + useconds/1000.0) + 0.5;

		printf("%li,%hi,%hi,%hi", elapsed, rawAcceleration.x, rawAcceleration.y, rawAcceleration.z);
	}
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

int main(int argc, char* argv[])
{
    const int port = I2CPort(argc, argv);
    WriteCSV_UntilControlC(port);
    return 0;
}
