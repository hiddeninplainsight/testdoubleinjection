#ifndef MOCK_I2C_H
#define MOCK_I2C_H

#include "HAL/I2C.h"

enum MockI2COperationType
{
	MockI2COperationType_SetAddress,
	MockI2COperationType_Read,
	MockI2COperationType_Write
};

struct MockI2COperation
{
	enum MockI2COperationType operation;
	unsigned char address;
	const void * buffer;
	int length;
	bool returnValue;
};

void MockI2C_Initialise(struct MockI2COperation* operations, int numberOfOperations);

void MockI2C_ExpectSetAddress(unsigned char address, bool returnValue);
void MockI2C_ExpectRead(const void * buffer, int length, bool returnValue);
void MockI2C_ExpectWrite(const void * buffer, int length, bool returnValue);

void MockI2C_Verify();

#endif // #ifndef MOCK_I2C_H
