#include "unity.h"

#include "Drivers/BMA150Accelerometer.h"
#include "Drivers/Raw3DSensorData.h"
#include "MockHAL/MockI2C.h"

const unsigned char deviceAddress = 0x38;

void setUp(void)
{
}

void tearDown(void)
{
}

void testBMA150Accelerometer_Reading_an_acceleration_of_0(void)
{
	// Given
	const int maxOperations = 10;
	struct MockI2COperation operations[maxOperations];
	MockI2C_Initialise(operations, maxOperations);

	const unsigned char readCommand[] = { 0x02 };
	const unsigned char readData[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	MockI2C_ExpectSetAddress(deviceAddress, true);
	MockI2C_ExpectWrite(readCommand, sizeof(readCommand), true);
	MockI2C_ExpectRead(readData, sizeof(readData), true);

	// When
	struct Raw3DSensorData result = BMA150Accelerometer_ReadAcceleration();

	// Then
	MockI2C_Verify();
	TEST_ASSERT_EQUAL(0, result.x);
	TEST_ASSERT_EQUAL(0, result.y);
	TEST_ASSERT_EQUAL(0, result.z);
}

void testBMA150Accelerometer_Reading_an_acceleration_of_1_in_all_directions(void)
{
	// Given
	const int maxOperations = 10;
	struct MockI2COperation operations[maxOperations];
	MockI2C_Initialise(operations, maxOperations);

	const unsigned char readCommand[] = { 0x02 };
	const unsigned char readData[] = { 0x40, 0x00, 0x40, 0x00, 0x40, 0x00 };

	MockI2C_ExpectSetAddress(deviceAddress, true);
	MockI2C_ExpectWrite(readCommand, sizeof(readCommand), true);
	MockI2C_ExpectRead(readData, sizeof(readData), true);

	// When
	struct Raw3DSensorData result = BMA150Accelerometer_ReadAcceleration();

	// Then
	MockI2C_Verify();
	TEST_ASSERT_EQUAL(1, result.x);
	TEST_ASSERT_EQUAL(1, result.y);
	TEST_ASSERT_EQUAL(1, result.z);
}

void testBMA150Accelerometer_Reading_different_accelerations_in_all_directions(void)
{
	// Given
	const int maxOperations = 10;
	struct MockI2COperation operations[maxOperations];
	MockI2C_Initialise(operations, maxOperations);

	const unsigned char readCommand[] = { 0x02 };
	const unsigned char readData[] = { 0xC0, 0xFF, 0x40, 0x00, 0x00, 0x00 };

	MockI2C_ExpectSetAddress(deviceAddress, true);
	MockI2C_ExpectWrite(readCommand, sizeof(readCommand), true);
	MockI2C_ExpectRead(readData, sizeof(readData), true);

	// When
	struct Raw3DSensorData result = BMA150Accelerometer_ReadAcceleration();

	// Then
	MockI2C_Verify();
	TEST_ASSERT_EQUAL(-1, result.x);
	TEST_ASSERT_EQUAL(1, result.y);
	TEST_ASSERT_EQUAL(0, result.z);
}
