#ifndef I2C_H
#define I2C_H

#include <stdbool.h>

bool I2C_SetAddress(unsigned char address);
bool I2C_Read(void * buffer, int length);
bool I2C_Write(const void * buffer, int length);

#endif // #ifndef I2C_H
