#include "BMA150Accelerometer.h"
#include "HAL/I2C.h"

struct Raw3DSensorData BMA150Accelerometer_ReadAcceleration(void)
{
	const unsigned char BMA150Address = 0x38;
	I2C_SetAddress(BMA150Address);

	const unsigned char registerAddress[] = { 0x02 };
	I2C_Write(registerAddress, sizeof(registerAddress));

	struct Raw3DSensorData rawAcceleration;
	I2C_Read(&rawAcceleration, sizeof(rawAcceleration));
	rawAcceleration.x >>= 6;
	rawAcceleration.y >>= 6;
	rawAcceleration.z >>= 6;

	return rawAcceleration;
}
