#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "RaspberryPiI2C.h"

static 	int file;

bool RaspberryPiI2C_Initialise(int port)
{
	if(port == 0)
		file = open("/dev/i2c-0", O_RDWR);
	else
		file = open("/dev/i2c-1", O_RDWR);
	return (file >= 0) ? true : false;
}

void RaspberryPiI2C_Close(void)
{
	close(file);
}

bool I2C_SetAddress(unsigned char address)
{
	return (ioctl(file, I2C_SLAVE, address) >= 0);
}

bool I2C_Read(void * buffer, int length)
{
	if(length > 0)
		return (read(file, buffer, length) == length);
	return false;
}

bool I2C_Write(const void * buffer, int length)
{
	if(length > 0)
		return (write(file, buffer, length) == length);
	return false;
}
