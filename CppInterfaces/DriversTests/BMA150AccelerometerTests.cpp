#include "unity.h"

#include "Drivers/BMA150Accelerometer.hpp"
#include "Drivers/Raw3DSensorData.hpp"
#include "MockHAL/MockI2C.hpp"

const unsigned char deviceAddress = 0x38;

void setUp()
{
}

void tearDown()
{
}

void testBMA150Accelerometer_Reading_an_acceleration_of_0()
{
	// Given
	const int maxOperations = 10;
	MockI2COperation operations[maxOperations];
	MockI2C i2c(operations, maxOperations);
	BMA150Accelerometer target(&i2c);

	const unsigned char readCommand[] = { 0x02 };
	const unsigned char readData[] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

	i2c.ExpectSetAddress(deviceAddress, true);
	i2c.ExpectWrite(readCommand, sizeof(readCommand), true);
	i2c.ExpectRead(readData, sizeof(readData), true);

	// When
	Raw3DSensorData result = target.ReadAcceleration();

	// Then
	i2c.Verify();
	TEST_ASSERT_EQUAL(0, result.x);
	TEST_ASSERT_EQUAL(0, result.y);
	TEST_ASSERT_EQUAL(0, result.z);
}

void testBMA150Accelerometer_Reading_an_acceleration_of_1_in_all_directions()
{
	// Given
	const int maxOperations = 10;
	MockI2COperation operations[maxOperations];
	MockI2C i2c(operations, maxOperations);
	BMA150Accelerometer target(&i2c);

	const unsigned char readCommand[] = { 0x02 };
	const unsigned char readData[] = { 0x40, 0x00, 0x40, 0x00, 0x40, 0x00 };

	i2c.ExpectSetAddress(deviceAddress, true);
	i2c.ExpectWrite(readCommand, sizeof(readCommand), true);
	i2c.ExpectRead(readData, sizeof(readData), true);

	// When
	Raw3DSensorData result = target.ReadAcceleration();

	// Then
	i2c.Verify();
	TEST_ASSERT_EQUAL(1, result.x);
	TEST_ASSERT_EQUAL(1, result.y);
	TEST_ASSERT_EQUAL(1, result.z);
}

void testBMA150Accelerometer_Reading_different_accelerations_in_all_directions()
{
	// Given
	const int maxOperations = 10;
	MockI2COperation operations[maxOperations];
	MockI2C i2c(operations, maxOperations);
	BMA150Accelerometer target(&i2c);

	const unsigned char readCommand[] = { 0x02 };
	const unsigned char readData[] = { 0xC0, 0xFF, 0x40, 0x00, 0x00, 0x00 };

	i2c.ExpectSetAddress(deviceAddress, true);
	i2c.ExpectWrite(readCommand, sizeof(readCommand), true);
	i2c.ExpectRead(readData, sizeof(readData), true);

	// When
	Raw3DSensorData result = target.ReadAcceleration();

	// Then
	i2c.Verify();
	TEST_ASSERT_EQUAL(-1, result.x);
	TEST_ASSERT_EQUAL(1, result.y);
	TEST_ASSERT_EQUAL(0, result.z);
}
