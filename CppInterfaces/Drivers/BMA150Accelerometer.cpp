#include "BMA150Accelerometer.hpp"
#include "Raw3DSensorData.hpp"
#include "HAL/I2C.hpp"

BMA150Accelerometer::BMA150Accelerometer(I2C *i2cPort)
	: i2c(i2cPort)
{
}

Raw3DSensorData BMA150Accelerometer::ReadAcceleration() const
{
	const unsigned char BMA150Address = 0x38;
	i2c->SetAddress(BMA150Address);

	const unsigned char registerAddress[] = { 0x02 };
	i2c->Write(registerAddress, sizeof(registerAddress));

	Raw3DSensorData rawAcceleration;
	i2c->Read(&rawAcceleration, sizeof(rawAcceleration));
	rawAcceleration.x >>= 6;
	rawAcceleration.y >>= 6;
	rawAcceleration.z >>= 6;

	return rawAcceleration;
}
