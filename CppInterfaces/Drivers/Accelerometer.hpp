#ifndef ACCELEROMETER_H
#define ACCELEROMETER_H

#include <exception>

struct Raw3DSensorData;

class Accelerometer
{
public:
	virtual ~Accelerometer() { }
	virtual Raw3DSensorData ReadAcceleration() const = 0;
};

class AccelerometerException : public std::exception
{
};

#endif // #ifndef ACCELEROMETER_H
