#ifndef BMA150_ACCELEROMETER_H
#define BMA150_ACCELEROMETER_H

#include "Accelerometer.hpp"

class I2C;

class BMA150Accelerometer : public Accelerometer
{
public:
	explicit BMA150Accelerometer(I2C *i2cPort);
	virtual Raw3DSensorData ReadAcceleration() const;

private:
	I2C *i2c;
};

class BMA150AccelerometerCommunicationsError : public AccelerometerException
{
};

#endif // #ifndef BMA150_ACCELEROMETER_H
