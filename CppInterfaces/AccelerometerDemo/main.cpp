#include <fstream>
#include <iostream>
#include <iomanip>
#include <sys/time.h>
#include <string.h>

#include "Drivers/BMA150Accelerometer.hpp"
#include "Drivers/Raw3DSensorData.hpp"
#include "RaspberryPiHardware/RaspberryPiI2C.hpp"

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

int I2CPort(int argc, char* argv[])
{
	for(int i = 1; i < argc; i++)
	{
		if(strcmp(argv[i], "-0") == 0)
			return 0;
	}
	return 1;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

struct CSV_xyz
{
	explicit CSV_xyz(const Raw3DSensorData & data)
		: data(data)
	{
	}
	Raw3DSensorData data;
};

CSV_xyz csv_xyz(const Raw3DSensorData & data)
{
	return CSV_xyz(data);
}

std::ostream & operator<<(std::ostream & os, const CSV_xyz & csv)
{
	os << ',' << csv.data.x
	   << ',' << csv.data.y
	   << ',' << csv.data.z;
	return os;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

struct CSV_xyz_title
{
	explicit CSV_xyz_title(const char * title)
		: title(title)
	{
	}
	const char * title;
};

CSV_xyz_title csv_xyz(const char * title)
{
	return CSV_xyz_title(title);
}

std::ostream & operator<<(std::ostream & os, const CSV_xyz_title & csv)
{
	os << ',' << csv.title << ".x"
	   << ',' << csv.title << ".y"
	   << ',' << csv.title << ".z";
	return os;
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
void WriteCSV_UntilControlC(std::ostream & ofs, int port)
{
	RaspberryPiI2C i2c(port);
	BMA150Accelerometer accelerometer(&i2c);

	ofs << std::fixed << std::setw(2) << std::setprecision(2)
	    << "time"
	    << csv_xyz("acceleration")
	    << std::endl;

	timeval start;
	gettimeofday(&start, NULL);

	while(true)
	{
		Raw3DSensorData rawAcceleration = accelerometer.ReadAcceleration();

		timeval current;
		gettimeofday(&current, NULL);

		const long seconds = current.tv_sec - start.tv_sec;
		const long useconds = current.tv_usec - start.tv_usec;
		const long elapsed = ((seconds) * 1000 + useconds/1000.0) + 0.5;

		ofs << elapsed
		    << csv_xyz(rawAcceleration)
		    << std::endl;

		ofs.flush();
	}
}

//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

int main(int argc, char* argv[])
{
    const int port = I2CPort(argc, argv);
    WriteCSV_UntilControlC(std::cout, port);
    return 0;
}
