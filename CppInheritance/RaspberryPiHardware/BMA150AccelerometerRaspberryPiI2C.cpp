#include <unistd.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <fcntl.h>

#include "HAL/I2C.hpp"
#include "BMA150AccelerometerRaspberryPiI2C.hpp"

BMA150AccelerometerRaspberryPiI2C::BMA150AccelerometerRaspberryPiI2C(int port)
{
	if(port == 0)
		file = open("/dev/i2c-0", O_RDWR);
	else
		file = open("/dev/i2c-1", O_RDWR);
	if(file < 0)
	{
		throw I2CUnableToOpenException();
	}
}

BMA150AccelerometerRaspberryPiI2C::~BMA150AccelerometerRaspberryPiI2C()
{
	close(file);
}

bool BMA150AccelerometerRaspberryPiI2C::SetAddress(unsigned char address)
{
	return (ioctl(file, I2C_SLAVE, address) >= 0);
}

bool BMA150AccelerometerRaspberryPiI2C::Read(void * buffer, int length)
{
	if(length > 0)
		return (read(file, buffer, length) == length);
	return false;
}

bool BMA150AccelerometerRaspberryPiI2C::Write(const void * buffer, int length)
{
	if(length > 0)
		return (write(file, buffer, length) == length);
	return false;
}
