#ifndef BMA150_ACCELEROMETER_RASPBERRY_PI_I2C_H
#define BMA150_ACCELEROMETER_RASPBERRY_PI_I2C_H

#include "Drivers/BMA150Accelerometer.hpp"

class BMA150AccelerometerRaspberryPiI2C : public BMA150Accelerometer
{
public:
	BMA150AccelerometerRaspberryPiI2C(int port);
	virtual ~BMA150AccelerometerRaspberryPiI2C();
protected:
	virtual bool SetAddress(unsigned char address);
	virtual bool Read(void * buffer, int length);
	virtual bool Write(const void * buffer, int length);
private:
	int file;
};

#endif // #ifndef BMA150_ACCELEROMETER_RASPBERRY_PI_I2C_H
