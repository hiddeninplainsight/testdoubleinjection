#ifndef I2C_H
#define I2C_H

#include <exception>

class I2CUnableToOpenException : public std::exception
{
};

#endif // #ifndef I2C_H
