#ifndef BMA150_ACCELEROMETER_H
#define BMA150_ACCELEROMETER_H

#include "Accelerometer.hpp"

struct Raw3DSensorData;

class BMA150Accelerometer
{
public:
	Raw3DSensorData ReadAcceleration();

protected:
	virtual bool SetAddress(unsigned char address) = 0;
	virtual bool Read(void * buffer, int length) = 0;
	virtual bool Write(const void * buffer, int length) = 0;
};

class BMA150AccelerometerCommunicationsError : public AccelerometerException
{
};

#endif // #ifndef BMA150_ACCELEROMETER_H
