#include "BMA150Accelerometer.hpp"
#include "Raw3DSensorData.hpp"

Raw3DSensorData BMA150Accelerometer::ReadAcceleration()
{
	const unsigned char BMA150Address = 0x38;
	SetAddress(BMA150Address);

	const unsigned char registerAddress[] = { 0x02 };
	Write(registerAddress, sizeof(registerAddress));

	Raw3DSensorData result;
	Read(&result, sizeof(result));
	result.x >>= 6;
	result.y >>= 6;
	result.z >>= 6;

	return result;
}
