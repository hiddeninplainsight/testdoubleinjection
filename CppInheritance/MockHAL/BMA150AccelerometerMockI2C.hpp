#ifndef BMA150_ACCELEROMETER_MOCK_I2C_H
#define BMA150_ACCELEROMETER_MOCK_I2C_H

#include "Drivers/BMA150Accelerometer.hpp"

struct MockI2COperationType
{
	enum value
	{
		SetAddress,
		Read,
		Write
	};
};

struct MockI2COperation
{
	MockI2COperationType::value operation;
	unsigned char address;
	const void * buffer;
	int length;
	bool returnValue;
};

class BMA150AccelerometerMockI2C : public BMA150Accelerometer
{
private:
	MockI2COperation* _operations;
	int _numberOfOperations;
	int _expectedOperations;

	int _currentOperation;

	void CheckOperation(MockI2COperationType::value operation, const char * message);
public:
	BMA150AccelerometerMockI2C(MockI2COperation* operations, int numberOfOperations);
	virtual ~BMA150AccelerometerMockI2C();

protected:
	virtual bool SetAddress(unsigned char address);
	virtual bool Read(void * buffer, int length);
	virtual bool Write(const void * buffer, int length);

public:
	void ExpectSetAddress(unsigned char address, bool returnValue);
	void ExpectRead(const void * buffer, int length, bool returnValue);
	void ExpectWrite(const void * buffer, int length, bool returnValue);

	void Verify();
};

#endif // #ifndef BMA150_ACCELEROMETER_MOCK_I2C_H
