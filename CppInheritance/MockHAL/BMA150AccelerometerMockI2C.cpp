#include "unity.h"
#include <memory.h>
#include <iostream>

#include "BMA150AccelerometerMockI2C.hpp"

BMA150AccelerometerMockI2C::BMA150AccelerometerMockI2C(MockI2COperation * operations, int numberOfOperations)
	: _operations(operations)
	, _numberOfOperations(numberOfOperations)
	, _expectedOperations(0)
	, _currentOperation(0)
{
}

BMA150AccelerometerMockI2C::~BMA150AccelerometerMockI2C()
{
	Verify();
}

void BMA150AccelerometerMockI2C::CheckOperation(MockI2COperationType::value operation, const char * message)
{
	TEST_ASSERT_TRUE_MESSAGE(_currentOperation < _expectedOperations, "Too many operations");

	TEST_ASSERT_EQUAL_MESSAGE(operation, _operations[_currentOperation].operation, message);
}

bool BMA150AccelerometerMockI2C::SetAddress(unsigned char address)
{
	CheckOperation(MockI2COperationType::SetAddress, "Unexpected I2C SetAddress operation");

	if(_operations[_currentOperation].address != address)
	{
		TEST_ASSERT_EQUAL_MESSAGE(address, _operations[_currentOperation].address, "Incorrect I2C address");
		return false;
	}

	bool result = _operations[_currentOperation].returnValue;

	_currentOperation++;
	return result;
}

bool BMA150AccelerometerMockI2C::Read(void * buffer, int length)
{
	CheckOperation(MockI2COperationType::Read, "Unexpected I2C Read operation");

	if(_operations[_currentOperation].length != length)
	{
		TEST_ASSERT_EQUAL_MESSAGE(length, _operations[_currentOperation].length, "Incorrect I2C read data length");
		return false;
	}

	memcpy(buffer, _operations[_currentOperation].buffer, length);
	bool result = _operations[_currentOperation].returnValue;

	_currentOperation++;
	return result;
}

bool BMA150AccelerometerMockI2C::Write(const void * buffer, int length)
{
	CheckOperation(MockI2COperationType::Write, "Unexpected I2C Write operation");

	if(_operations[_currentOperation].length != length)
	{
		TEST_ASSERT_EQUAL_MESSAGE(length, _operations[_currentOperation].length, "Incorrect I2C write data length");
		return false;
	}

	if(memcmp(buffer, _operations[_currentOperation].buffer, length))
	{
		TEST_ASSERT_EQUAL_MESSAGE(0, memcmp(buffer, _operations[_currentOperation].buffer, length), "Incorrect data written to the I2C port");
		return false;
	}

	bool result = _operations[_currentOperation].returnValue;

	_currentOperation++;

	return result;
}

void BMA150AccelerometerMockI2C::ExpectSetAddress(unsigned char address, bool returnValue)
{
	_operations[_expectedOperations].operation = MockI2COperationType::SetAddress;
	_operations[_expectedOperations].address = address;
	_operations[_expectedOperations].returnValue = returnValue;
	_expectedOperations++;
	TEST_ASSERT_TRUE_MESSAGE(_expectedOperations <= _numberOfOperations, "Attempted to add too many operations to the I2C Mock");
}

void BMA150AccelerometerMockI2C::ExpectRead(const void * buffer, int length, bool returnValue)
{
	_operations[_expectedOperations].operation = MockI2COperationType::Read;
	_operations[_expectedOperations].buffer = buffer;
	_operations[_expectedOperations].length = length;
	_operations[_expectedOperations].returnValue = returnValue;
	_expectedOperations++;
	TEST_ASSERT_TRUE_MESSAGE(_expectedOperations <= _numberOfOperations, "Attempted to add too many operations to the I2C Mock");
}

void BMA150AccelerometerMockI2C::ExpectWrite(const void * buffer, int length, bool returnValue)
{
	_operations[_expectedOperations].operation = MockI2COperationType::Write;
	_operations[_expectedOperations].buffer = buffer;
	_operations[_expectedOperations].length = length;
	_operations[_expectedOperations].returnValue = returnValue;
	_expectedOperations++;
	TEST_ASSERT_TRUE_MESSAGE(_expectedOperations <= _numberOfOperations, "Attempted to add too many operations to the I2C Mock");
}

void BMA150AccelerometerMockI2C::Verify()
{
	TEST_ASSERT_EQUAL_MESSAGE(_expectedOperations, _currentOperation, "Not all expected I2C operations have occured");
}
