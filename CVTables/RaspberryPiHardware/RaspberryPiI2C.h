#ifndef RASPBERRYPI_I2C_H
#define RASPBERRYPI_I2C_H

#include "HAL/I2C.h"

bool RaspberryPiI2C_Initialise(int port);
void RaspberryPiI2C_Close(void);

extern const struct I2C RaspberryPiI2C;

#endif // #ifndef RASPBERRYPI_I2C_H
