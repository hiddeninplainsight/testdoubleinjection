#include "unity.h"
#include <memory.h>

#include "MockI2C.h"

static struct MockI2COperation* _operations;
static int _numberOfOperations;
static int _expectedOperations;

static int _currentOperation;


void MockI2C_Initialise(struct MockI2COperation * operations, int numberOfOperations)
{
	_operations = operations;
	_numberOfOperations = numberOfOperations;
	_expectedOperations = 0;
	_currentOperation = 0;
}


static void CheckOperation(enum MockI2COperationType operation, const char * message)
{
	TEST_ASSERT_TRUE_MESSAGE(_currentOperation < _expectedOperations, "Too many operations");

	TEST_ASSERT_EQUAL_MESSAGE(operation, _operations[_currentOperation].operation, message);
}

static bool MockI2C_SetAddress(unsigned char address)
{
	CheckOperation(MockI2COperationType_SetAddress, "Unexpected I2C SetAddress operation");

	if(_operations[_currentOperation].address != address)
	{
		TEST_ASSERT_EQUAL_MESSAGE(address, _operations[_currentOperation].address, "Incorrect I2C address");
		return false;
	}

	bool result = _operations[_currentOperation].returnValue;

	_currentOperation++;
	return result;
}

static bool MockI2C_Read(void * buffer, int length)
{
	CheckOperation(MockI2COperationType_Read, "Unexpected I2C Read operation");

	if(_operations[_currentOperation].length != length)
	{
		TEST_ASSERT_EQUAL_MESSAGE(length, _operations[_currentOperation].length, "Incorrect I2C read data length");
		return false;
	}

	memcpy(buffer, _operations[_currentOperation].buffer, length);
	bool result = _operations[_currentOperation].returnValue;

	_currentOperation++;
	return result;
}

static bool MockI2C_Write(const void * buffer, int length)
{
	CheckOperation(MockI2COperationType_Write, "Unexpected I2C Write operation");

	if(_operations[_currentOperation].length != length)
	{
		TEST_ASSERT_EQUAL_MESSAGE(length, _operations[_currentOperation].length, "Incorrect I2C write data length");
		return false;
	}

	if(memcmp(buffer, _operations[_currentOperation].buffer, length))
	{
		TEST_ASSERT_EQUAL_MESSAGE(0, memcmp(buffer, _operations[_currentOperation].buffer, length), "Incorrect data written to the I2C port");
		return false;
	}

	bool result = _operations[_currentOperation].returnValue;

	_currentOperation++;

	return result;
}

void MockI2C_ExpectSetAddress(unsigned char address, bool returnValue)
{
	_operations[_expectedOperations].operation = MockI2COperationType_SetAddress;
	_operations[_expectedOperations].address = address;
	_operations[_expectedOperations].returnValue = returnValue;
	_expectedOperations++;
	TEST_ASSERT_TRUE_MESSAGE(_expectedOperations <= _numberOfOperations, "Attempted to add too many operations to the I2C Mock");
}

void MockI2C_ExpectRead(const void * buffer, int length, bool returnValue)
{
	_operations[_expectedOperations].operation = MockI2COperationType_Read;
	_operations[_expectedOperations].buffer = buffer;
	_operations[_expectedOperations].length = length;
	_operations[_expectedOperations].returnValue = returnValue;
	_expectedOperations++;
	TEST_ASSERT_TRUE_MESSAGE(_expectedOperations <= _numberOfOperations, "Attempted to add too many operations to the I2C Mock");
}

void MockI2C_ExpectWrite(const void * buffer, int length, bool returnValue)
{
	_operations[_expectedOperations].operation = MockI2COperationType_Write;
	_operations[_expectedOperations].buffer = buffer;
	_operations[_expectedOperations].length = length;
	_operations[_expectedOperations].returnValue = returnValue;
	_expectedOperations++;
	TEST_ASSERT_TRUE_MESSAGE(_expectedOperations <= _numberOfOperations, "Attempted to add too many operations to the I2C Mock");
}

void MockI2C_Verify()
{
	TEST_ASSERT_EQUAL_MESSAGE(_expectedOperations, _currentOperation, "Not all expected I2C operations have occured");
}

const struct I2C MockI2C =
{
	.SetAddress = MockI2C_SetAddress,
	.Read = MockI2C_Read,
	.Write = MockI2C_Write
};
