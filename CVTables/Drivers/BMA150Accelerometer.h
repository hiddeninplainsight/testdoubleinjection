#ifndef BMA150_ACCELEROMETER_H
#define BMA150_ACCELEROMETER_H

#include "Accelerometer.h"

struct I2C;

void BMA150Accelerometer_Initialise(const struct I2C *i2cPort);

extern const struct Accelerometer BMA150Accelerometer;

#endif // #ifndef BMA150_ACCELEROMETER_H
