#include "BMA150Accelerometer.h"
#include "Raw3DSensorData.h"
#include "HAL/I2C.h"

static const struct I2C *i2c;

void BMA150Accelerometer_Initialise(const struct I2C *i2cPort)
{
	i2c = i2cPort;
}

static struct Raw3DSensorData BMA150Accelerometer_ReadAcceleration(void)
{
	const unsigned char BMA150Address = 0x38;
	i2c->SetAddress(BMA150Address);

	const unsigned char registerAddress[] = { 0x02 };
	i2c->Write(registerAddress, sizeof(registerAddress));

	struct Raw3DSensorData rawAcceleration;
	i2c->Read(&rawAcceleration, sizeof(rawAcceleration));
	rawAcceleration.x >>= 6;
	rawAcceleration.y >>= 6;
	rawAcceleration.z >>= 6;

	return rawAcceleration;
}

const struct Accelerometer BMA150Accelerometer =
{
	.ReadAcceleration = BMA150Accelerometer_ReadAcceleration
};
