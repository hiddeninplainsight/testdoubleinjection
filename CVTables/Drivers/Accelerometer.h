#ifndef ACCELEROMETER_H
#define ACCELEROMETER_H

struct Raw3DSensorData;

struct Accelerometer
{
	struct Raw3DSensorData (*ReadAcceleration)(void);
};

#endif // #ifndef ACCELEROMETER_H
