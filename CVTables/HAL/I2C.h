#ifndef I2C_H
#define I2C_H

#include <stdbool.h>

struct I2C
{
	bool (*SetAddress)(unsigned char address);
	bool (*Read)(void * buffer, int length);
	bool (*Write)(const void * buffer, int length);
};

#endif // #ifndef I2C_H
